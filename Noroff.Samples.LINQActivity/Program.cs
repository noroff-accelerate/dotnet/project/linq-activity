﻿namespace Noroff.Samples.LINQActivity
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var books = new List<Book>
            {
                new Book { Title = "Book 1", Author = "Author A", YearPublished = 2001 },
                new Book { Title = "Book 2", Author = "Author B", YearPublished = 1999 },
                // Add more books here
            };

            var booksAfter2000 = books.Where(book => book.YearPublished > 2000).ToList();

            var booksByAuthor = (from book in books
                                 where book.Author == "Author A"
                                 orderby book.YearPublished
                                 select book).ToList();

            int authorBookCount = books.Count(book => book.Author == "Author A");

            Console.WriteLine("Books published after 2000:");
            foreach (var book in booksAfter2000)
            {
                Console.WriteLine($"{book.Title} ({book.YearPublished})");
            }

            Console.WriteLine("\nBooks by Author A:");
            foreach (var book in booksByAuthor)
            {
                Console.WriteLine($"{book.Title} ({book.YearPublished})");
            }

            Console.WriteLine($"\nTotal books by Author A: {authorBookCount}");

        }
    }

    public class Book
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public int YearPublished { get; set; }
    }

}