# LINQ Activity: Library Management System

This activity involves using LINQ (Language-Integrated Query) to perform various queries in a simulated library management system.

## Objective
- Understand and apply LINQ queries in C#.
- Practice both method and query expression syntax in LINQ.
- Enhance skills in data filtering, ordering, and aggregation.

## Environment Setup
- Ensure you have a C# development environment set up, like Visual Studio or VS Code with .NET SDK.

## Steps

### 1. Create the Book Class
- Define a `Book` class with properties like `Title`, `Author`, and `YearPublished`.

### 2. Initialize a List of Books
- Create a list of `Book` objects, representing a variety of books in a library.

### 3. LINQ Query Using Method Syntax
- Write a query to find books published after the year 2000.

### 4. LINQ Query Using Query Expression Syntax
- Construct a query to select books by a specific author and order them by publication year.

### 5. Count the Number of Books by an Author
- Use a LINQ method to calculate the total number of books by a chosen author.

### 6. Display the Results
- Output the results of your queries to the console.

## Observations
- Note the differences between method and query expression syntax in LINQ.
- Observe how LINQ enhances code readability compared to traditional looping constructs.
- Experiment with LINQ's querying capabilities on in-memory collections.

## Conclusion
This activity demonstrates the versatility and power of LINQ in C#, making it an essential skill for effective data manipulation and querying in .NET applications.
